import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../components/pages/Home';
import Details from "../components/pages/Details";
import Favourites from "../components/pages/Favourites";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/details/:id',
        name: 'details',
        component: Details
    },
    {
        path: '/favourites',
        name: 'favourites',
        component: Favourites
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
