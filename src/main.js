import Vue from 'vue'
import App from './components/App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from "./vuetify";
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.config.productionTip = false;
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBML1Oxo2DsvORumgtdY3VqEmkKgMXqb8U',
        libraries: 'places'
    }
});

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')
