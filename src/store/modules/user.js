import api from '../../api';

const state = () => ({
    users: [],
    favourites: []
})

const getters = {
    users: (state) => {
        return state.users;
    },
    favourites: (state) => {
        return state.users.filter(u => state.favourites.includes(u.localId));
    }
}

const actions = {
    loadUser({commit}) {
        return api.getUser().then(user => {
            commit('setUser', user);
        });
    },
    loadUsers({commit}) {
        commit('setFavourites', []);
        return api.getUsers().then(users => {
            let id = 1;
            commit('setUsers', users.map(u => {
                return {
                    ...u,
                    localId: id++
                }
            }));
        });
    },
    addFavourite({commit}, user) {
        commit('addFavourite', user);
    },
    removeFavourite({commit}, user) {
        commit('removeFavourite', user);
    }
}

const mutations = {
    setUsers(state, users) {
        state.users = users;
    },
    addFavourite(state, user) {
        state.favourites.push(user.localId);
    },
    removeFavourite(state, user) {
        state.favourites = state.favourites.filter(f => f !== user.localId);
    },
    setFavourites(state, users) {
        state.favourites = users.map(u => u.localId);
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
