import axios from 'axios';

export default {
    getUsers() {
        return axios.get('https://randomuser.me/api/?results=100').then(response => {
            let data = response.data;
            return data.results;
        });
    }
}
